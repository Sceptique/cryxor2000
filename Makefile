##
## Makefile for  in /home/poulet_a/projets/cryxor2000
## 
## Made by poulet_a
## Login   <poulet_a@epitech.net>
## 
## Started on  Fri Feb 14 12:15:50 2014 poulet_a
## Last update Fri Feb 14 12:34:52 2014 poulet_a
##

CC	=	cc

RM	=	rm -f

CFLAGS  +=      -I. -Ilibs -Llibs -lmy
CFLAGS	+=	-Wall -Wextra -Wextra -pedantic

NAME	=	cryxor2000

SRCS	=	src/cryxor.c

LIBS_DIR=	std_epitech

all:		libs $(NAME)

libs:
		@(cd $(LIBS_DIR) && $(MAKE))
		@(cd $(LIBS_DIR) && $(MAKE) libs)

$(NAME):	$(OBJS)
		cc $(SRCS) -o $(NAME) $(CFLAGS)

clean:
		@(cd $(LIBS_DIR) && $(MAKE) $@)
		$(RM) $(OBJS)

fclean:		clean
		@(cd $(LIBS_DIR) && $(MAKE) $@)
		@(cd $(LIBS_DIR) && $(MAKE) rmlibs)
		$(RM) $(NAME)

re:		fclean all
		@(cd $(LIBS_DIR) && $(MAKE) relibs)

.PHONY:		clean fclean all re libs
